interface Book{
    fun getInfo()
    fun order()
    fun rate()
}

enum class Genre{
    SCIENCE, LITERATURE
}

class BookFactory{

    companion object {
        fun createBook(genre: Genre):Book = when(genre){
            Genre.SCIENCE -> object: Book{
                private val title = "Teoria de la relatividad"
                override fun getInfo() {
                    println("$title fue escrita por Albert Einstein")
                }

                override fun order() {
                    println("Order $title")
                }

                override fun rate() {
                    println("Rate for $title")
                }
            }
            Genre.LITERATURE -> object: Book{
                private val title = "El gran Gatsby"
                override fun getInfo() {
                    println("$title fue escrita por F. Scott Fitzgerald")
                }

                override fun order() {
                    println("Order $title")
                }

                override fun rate() {
                    println("Rate for $title")
                }
            }
        }
    }
}