class Singleton (var value:String = "mongo"){
    companion object{
        private var instance:Singleton ? = null
        fun getInstance():Singleton{
            if(instance == null){
                instance = Singleton()
            }
            return instance!!
       }
    }
}

fun main(args: Array<String>){
    val singleton = Singleton.getInstance()
    print(singleton)
}