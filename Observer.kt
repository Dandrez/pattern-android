class Foco{
    private lateinit var idFoco:String
    private var sensores = ArrayList<Sensor>()
    private lateinit var estado:String

    constructor(idFoco:String){
        this.idFoco=idFoco
    }

    public fun suscrirObservador(sensor: Sensor){
        sensores.add(sensor)
    }

    public fun eliminarObservador(sensor: Sensor){
        sensores.remove(sensor)
    }

    public fun notificarObservadores(){
        sensores.forEach {
            it.notificar()
        }
    }

    public fun getUltimoSuceso():String{
        return idFoco+" : "+estado
    }

    public fun setUltimoSuceso(estado:String){
        this.estado=estado
        notificarObservadores()
    }

}

class Sensor{
    private lateinit var idSensor:String
    private lateinit var foco:Foco
    constructor(idSensor: String, foco:Foco){
        this.foco = foco
        this.idSensor = idSensor
    }
    public fun notificar(){
        println(idSensor+"  "+foco.getUltimoSuceso())
    }
}

fun main(args: Array<String>){
    var foco1 = Foco("F1")
    var sensorPiso = Sensor("t1",foco1)
    foco1.suscrirObservador(sensorPiso)
    var sensorTecho = Sensor("s1",foco1)
    foco1.suscrirObservador(sensorTecho)
    foco1.setUltimoSuceso("Encendido")
    foco1.eliminarObservador(sensorTecho)
    foco1.setUltimoSuceso("Apagado")
}


