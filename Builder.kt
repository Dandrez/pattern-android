abstract class Builder {
    open fun funda() {}
    open fun relleno() {}
    open fun color() {}
}

class BuilderAlmohadaDePluma: Builder(){
    private var producto = AlmohadaDePlusmas()
    fun reset(){
        producto = AlmohadaDePlusmas()
    }
    override fun funda(){
        producto.add("franela")
    }
    override fun relleno(){
        producto.add("plumas de ganzo")
    }
    override fun color(){
        producto.add("blaca")
    }
    fun recuperarProducto(): AlmohadaDePlusmas{
        return this.producto
    }
}

class AlmohadaDePlusmas{
    val partes = mutableListOf<String>()
    fun add(parte:String){
        this.partes.add(parte)
    }
    fun listarPartes(): String{
        return "Partes de la almohada " + partes.joinToString(separator = ", ")
    }
}

class Director{
    private var builder:Builder? = null
    fun update(builder: Builder){
        this.builder = builder
    }
    fun buildFullFeaturedAlmohada(){
        builder?.funda()
        builder?.relleno()
        builder?.color()
    }
}

class Cliente(){
    fun someClientCode(director: Director){
        var builder = BuilderAlmohadaDePluma()
        director.update(builder)

        println("Almohada")
        director.buildFullFeaturedAlmohada()
        print(builder.recuperarProducto().listarPartes())
    }
}

fun main(args: Array<String>){
    var director = Director()
    var cliente = Cliente()
    cliente.someClientCode(director)
}