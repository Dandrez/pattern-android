interface Combo{
    fun getPrecio():Float
    fun getDescription():String
}

class ComboCompleto: Combo{
    override fun getPrecio(): Float {
        return 4.50F
    }

    override fun getDescription(): String {
        return "2 presas + 1 papa frita pequena + 1 bebida (400 ml) + 1 moncaiba"
    }

}

class ComboIdeal: Combo{
    override fun getPrecio(): Float {
    return 6.50F
    }

    override fun getDescription(): String {
        return "3 presas + 1 papa frita pequena + 1 bebida (400 ml) + 1 moncaiba"
    }
}

class GranCombo: Combo{
    override fun getPrecio(): Float {
        return 9.60F
    }

    override fun getDescription(): String {
        return "5 presas + 1 papa frita grande + 1 bebida (400 ml) + 2 moncaiba"
    }
}

abstract class complemento:Combo{
    private var comboDecorado: Combo? = null
    constructor(comboDecorado: Combo){
        this.comboDecorado = comboDecorado
    }

    abstract override fun getPrecio(): Float

    abstract override fun getDescription(): String

}

class DecoratorPapasFritas: complemento {
    private var comboDecorado: Combo? = null
    constructor(comboDecorado:Combo) : super(comboDecorado) {
        this.comboDecorado = comboDecorado
    }

    override fun getPrecio(): Float {
        return comboDecorado!!.getPrecio() + 0.90F
    }

    override fun getDescription(): String {
        return comboDecorado!!.getDescription() + " + porcio de papas fritas medianas"
    }

}

class DecoratorEnsalada: complemento {
    private var comboDecorado: Combo? = null
    constructor(comboDecorado:Combo) : super(comboDecorado) {
        this.comboDecorado = comboDecorado
    }

    override fun getPrecio(): Float {
        return comboDecorado!!.getPrecio() + 0.99F
    }

    override fun getDescription(): String {
        return comboDecorado!!.getDescription() + " + ensalada de col med."
    }

}

class DecoratorArroz: complemento {
    private var comboDecorado: Combo? = null
    constructor(comboDecorado:Combo) : super(comboDecorado) {
        this.comboDecorado = comboDecorado
    }

    override fun getPrecio(): Float {
        return comboDecorado!!.getPrecio() + 1.30F
    }

    override fun getDescription(): String {
        return comboDecorado!!.getDescription() + " + porcion de arroz"
    }

}

fun main(args: Array<String>) {
    var  comboIdealConArroz:Combo = DecoratorArroz(ComboIdeal())
    println("Descripcion: "+comboIdealConArroz.getDescription())
    println("Precio: "+comboIdealConArroz.getPrecio())
}
